export const configurationTabs = [
  {
    title: 'I want some project management features',
    name: 'management',
  },
  {
    title: 'I want some deployment features',
    name: 'deployment',
    labelInput: {
      title: 'I want to deploy to a subdomain',
      id: 'subdomain',
      target: 'subdomain',
    },
  },
  {
    title: 'I want some security features',
    name: 'security',
  },
  {
    title: 'I want some impact measurement',
    name: 'performance',
  },
];
