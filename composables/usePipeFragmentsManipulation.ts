import { PipeFragment, PipeFragments } from '~/models/gitlab-ci';
import { ActionAndConditions } from '~/models/tags';
import { pipeFragments } from '~/data/gitlab-ci/pipeFragments';

const pipeFragmentRef = ref<PipeFragments>(pipeFragments);

export default function usePipeFragmentsManipulation() {
  const selectedFiles = computed(() => {
    return pipeFragmentRef.value.reduce<string[]>(
      (pipes, pipe) =>
        pipe.isSelected
          ? [...pipes, pipe.file.replace('#options', pipe.selectedOption?.value)]
          : [...pipes],
      [],
    );
  });

  const selectedTabs = computed(() => {
    return pipeFragmentRef.value.reduce<string[]>(
      (pipes, pipe) => (pipe.isSelected ? [...pipes, pipe.tab] : [...pipes]),
      [],
    );
  });

  const selectedFilesUuids = computed(() => {
    return {
      files: pipeFragmentRef.value.reduce<string[]>(
        (pipes, pipe) => (pipe.isSelected ? [...pipes, pipe.uuid] : [...pipes]),
        [],
      ),
    };
  });

  const selectedOptions = computed(() => {
    return {
      options: pipeFragmentRef.value.reduce<string[]>(
        (pipes, pipe) => (pipe.isSelected ? [...pipes, pipe.selectedOption?.query] : [...pipes]),
        [],
      ),
    };
  });

  const isOneHelmPipeSelected = () => {
    return selectedFiles.value.some((elem) => elem.includes('helm'));
  };

  const isActionDisplayed = (action: ActionAndConditions) => {
    if (action.stage === 'deploy') {
      return isOneHelmPipeSelected();
    }
    return (!action.stage && !action.file) || selectedFiles.value.includes(action.file);
  };

  const isDisabled = (pipe: PipeFragment) => {
    return pipe.need ? !pipe.need.every((elem) => selectedFiles.value.includes(elem)) : false;
  };

  return {
    pipeFragmentRef,
    selectedFiles,
    selectedTabs,
    selectedFilesUuids,
    selectedOptions,
    isOneHelmPipeSelected,
    isActionDisplayed,
    isDisabled,
  };
}
